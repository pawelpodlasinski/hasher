FROM php:7.1-alpine

RUN apk add --no-cache curl git bash

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY --chown=1000:1000 . /data

WORKDIR /data

RUN composer install

EXPOSE 1201

CMD ["/data/run.sh"]