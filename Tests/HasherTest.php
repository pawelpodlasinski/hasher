<?php

namespace Tests;

use Gaffa\Hasher;
use PHPUnit\Framework\TestCase;

class HasherTest extends TestCase
{
    /** @var Hasher $hasher */
    private $hasher;

    public function setUp()
    {
        parent::setUp();
        $this->hasher = new Hasher();
    }

    /**
     * @test
     * @dataProvider saltProvider
     * @param string $input
     * @param int $expected
     */
    public function generate_salt_length(string $input, int $expected)
    {
        $this->assertEquals($expected, $this->hasher->generateSaltLength($input));
    }

    public function saltProvider()
    {
        return [
            ['moje_super_tajne_haslo_do_konta_hej', 18],
            ['abc', 2],
            ['test', 2],
            ['superLongPassword', 9],
            ['superExtraLongPasswordNotComplexedAtAllButIDontCareAtTheMoment', 31],
            ['sup3rL0ngAndC0mpl3xPassw0rd', 14],
            ['098f6bcd4621d373cade4e832627b4f6', 16],
            ['a', 1],
            ['okoń', 3],
            ['śćółką łączył świteź', 15]
        ];
    }

    /**
     * @test
     * @dataProvider stringProvider
     * @param string $input
     * @param string $expected
     */
    public function encode_should_return_value(string $input, string $expected)
    {
        $this->assertEquals($expected[0], substr($this->hasher->encode($input), 0, 1));
        $this->assertNotEquals($expected[1], substr($this->hasher->encode($input), 1, 1));
    }

    public function stringProvider()
    {
        return [
            ['moje_super_tajne_haslo_do_konta_hej', 'mUoj`e_XsuXpeFr_Rta&jnxe_DhaMsl$o_Wdo&_kmon?tad_h]ejK'],
            ['aJbc~', 'abc'],
            ['tfesht', 'test'],
            ['sJup,er9Lo*ng^Pa2ss(wozrdN', 'superLongPassword'],
            ['svup-erJEx2tr1aLEonNgPzasmsw`or]dNUot+CoYmp*lexxe$dARtAbllUBu^tI!DoOnt}Ca`reDAtTThUeM6omzen?t', 'superExtraLongPasswordNotComplexedAtAllButIDontCareAtTheMoment'],
            ['stupe3r#L0mngPAn-dC$0m5plD3x_Pa2ssdw0!rd_', 'sup3rL0ngAndC0mpl3xPassw0rd'],
            ['0i98xf65bc,d4$62(1do37j3c4ad]e4Le8p32H62$7bv4fD6', '098f6bcd4621d373cade4e832627b4f6'],
            ['a1', 'a'],
            ['oVko8ń{', 'okoń']
        ];
    }
}
