<?php

namespace Tests;

use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\TestListenerDefaultImplementation;

class ProfilingTestListener implements TestListener
{
    use TestListenerDefaultImplementation;

    public function endTest(Test $test, float $time): void
    {
        printf(
            "\nTest '%s' ended.\tTest time %s s.\n",
            str_pad($test->toString(), 70),
            number_format($time, 3)
        );
    }
}
