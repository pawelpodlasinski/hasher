<?php

namespace Gaffa;

class Hasher
{
    private $hashLevel = 2;
    private $hashShowSigns = 2;
    private $hashChars = "poiuytrewq0987654321POIUYTREWQlkjhgfds_+-={}[],.?MNBVCXZ)(*&^%$#@!`~LKJHGFDSAamnbvcxz";

    /**
     * Hasher constructor.
     * @param int $hashLevel
     * @param int $hashShowSigns
     */
    public function __construct(int $hashLevel = 2, int $hashShowSigns = 2)
    {
        $this->hashLevel = $hashLevel;
        $this->hashShowSigns = $hashShowSigns;
    }

    /**
     * Hash string with random characters
     * @param $string
     * @return string
     */
    public function encode(string $string): string
    {
        $saltLength = $this->generateSaltLength($string);

        for ($i = $saltLength; $i > 0; $i--) {
            $salt = $this->hashChars[rand(0, strlen($this->hashChars) - 1)];
            $position = ($i * $this->hashLevel) - 1;
            $string = substr_replace($string, $salt, $position, 0);
        }

        return $string;
    }

    /**
     * Return hashed string to original
     * @param $string
     * @return string
     */
    public function decode($string): string
    {
        $output = "";
        $c = 1;
        for ($i = 0; $i < strlen($string); $i++) {
            if ($i !== $c) {
                $output .= $string[$i];
            } else {
                $c += $this->hashLevel +1;
            }
        }

        return $output;
    }

    /**
     * Mask string with stars
     * @param $string
     * @return string
     */
    public function displayAsHash($string): string
    {
        for ($i = 0; $i < strlen($string); $i++) {
            if ($i < $this->hashShowSigns || $i >= strlen($string) - $this->hashShowSigns) {
                continue;
            }

            $string = substr_replace($string, "*", $i, 1);
        }

        return $string;
    }

    /**
     * @param string $string
     * @return int
     */
    public function generateSaltLength(string $string): int
    {
        return round((strlen($string) + .5) / $this->hashLevel, 0);
    }
}
